//
//  MVVMListViewTests.swift
//  MVVMListDetailViewTests
//
//  Created by Alistair Priest on 08/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import XCTest
@testable import MVVMListDetailView

class FakeRouter : Routing {
    var presentedDetailItem: Item?
    
    func presentDetailView(for item: Item) {
        self.presentedDetailItem = item
    }
}

class MVVMListViewTests: XCTestCase {
    
    var router: FakeRouter?
    var factory: ListViewViewModelFactory?
    var items: [Item]?
    
    override func setUp() {
        let router = FakeRouter()
        self.factory = ListViewViewModelFactory(router: router)
        self.items = TestItemFactory().buildDummyItems(count: 3)
        self.router = router
    }
    
    func testListViewViewModelProvidesItemCountFromModel() {
        let sut = self.factory!.build(items: self.items!)
        XCTAssertEqual(sut.getItemCount, 3)
    }
    
    func testImageUrlForFirstItemMatches() {
        let sut = self.factory!.build(items: self.items!)
        XCTAssertEqual(self.items!.first?.imageUrl, sut.getItemViewModel(at: 0)?.imageUrl.absoluteString)
    }
    
    func testSelectingRowInformsRouterWithCorrectModel() {
        let sut = self.factory!.build(items: self.items!)
        sut.didSelectRow(at: 1)
        XCTAssertEqual(self.router?.presentedDetailItem?.title, self.items![1].title)
    }
    
}
