//
//  MVVMListDetailViewTests.swift
//  MVVMListDetailViewTests
//
//  Created by Alistair Priest on 08/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import XCTest
import UIKit
@testable import MVVMListDetailView

class TestItemFactory {
    func buildDummyItems(count: Int) -> [Item] {
        var items: [Item] = [Item]()
        for i in (0..<count) {
            items.append(Item(title: "title - \(i)", imageUrl: "http://foo.com/image.gif", showImage: true))
        }
        return items
    }
}

class MVVMListDetailViewTests: XCTestCase {
    let sampleItem = Item(title: "title", imageUrl: "http://foo.com/image.gif", showImage: true)
    
    func testViewModelProvidesTitleFromModel() {
        let factory = DetailViewViewModelFactory()
        let sut = factory.build(item: sampleItem)
        XCTAssertEqual(sut.title, "title")
    }

    func testViewModelProvidesImageFromModel() {
        let factory = DetailViewViewModelFactory()
        let sut = factory.build(item: sampleItem)
        XCTAssertEqual(sut.imageUrl.absoluteString, sampleItem.imageUrl)
    }

}
