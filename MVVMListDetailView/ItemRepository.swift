//
//  ItemRepository.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 07/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

class ItemRepository {
    func getItems() -> [Item] {
        let item = Item(title: "hello", imageUrl: "https://placem.at/people?w=410&h=320", showImage: true)
        let item2 = Item(title: "goodbye", imageUrl: "https://placem.at/people?w=150&h=150&random=1", showImage: true)
        let item3 = Item(title: "welcome", imageUrl: "https://placem.at/people?w=150&h=150&random=1", showImage: false)

        return [item, item2, item3]
    }
}
