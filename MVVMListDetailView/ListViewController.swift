//
//  ListViewController.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 06/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit

class ListViewController: UITableViewController {
    
    public var viewModel:ListViewViewModel?
    
    override func viewDidLoad() {
        self.viewModel?.reloadTableViewClosure = { [unowned self] in
            self.tableView.reloadData()
        }
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel?.getItemCount ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ListViewItemTableViewCell
        
        if let item = self.viewModel?.getItemViewModel(at: indexPath.row) {
            cell.productImage?.loadAsync(fromUrl: item.imageUrl)
            cell.productImage?.isHidden = !item.showImage
            cell.name?.text = item.title
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel?.didSelectRow(at: indexPath.row)
    }
}

class ListViewItemTableViewCell: UITableViewCell {
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var productImage: UIImageView!
}

class ListViewViewModel: ViewControllerProviding {
    var controller: UIViewController
    private var router: Routing
    var reloadTableViewClosure: (() -> ())?
    private var model: [Item]
    
    init(_ router: Routing, controller: UIViewController, model: [Item]) {
        self.controller = controller
        self.router = router
        self.model = model

        self.viewModels = model.map { (item) in
            return ItemViewModel(title: item.title, imageUrl: URL(string: item.imageUrl)!, showImage: item.showImage)
        }
    }
    
    func getItemViewModel(at row: Int) -> ItemViewModel? {
        guard let models = self.viewModels else {
            return nil
        }
        return models[row]
    }
    
    private var viewModels: [ItemViewModel]? {
        didSet {
            self.reloadTableViewClosure?()
        }
    }

    var getItemCount:Int {
        get {
            if let models = self.viewModels {
                return models.count
            }
            return 0
        }
    }
    
    func didSelectRow(at row: Int) {
        self.router.presentDetailView(for: self.model[row])
    }
}

class ListViewViewModelFactory {
    
    private var router:Routing
    
    init(router: Routing) {
        self.router = router
    }
    
    func build(items: [Item]) -> ListViewViewModel {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.loadListViewController()
        let viewModel = ListViewViewModel(self.router, controller: controller, model: items)
        controller.viewModel = viewModel
        return viewModel
    }
}

extension UIImageView {
    func loadAsync(fromUrl imageUrl: URL) {
        // this would be implemented by sdwebimage, or the alamafire image extensions
        do {
            let data = try Data(contentsOf: imageUrl)
            self.image = UIImage(data: data)!
        } catch {
            // ignore
        }
    }
}
