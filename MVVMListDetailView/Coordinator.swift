//
//  Coordinator.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 07/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit

protocol Coordinating {
    func present(_ viewModel: ViewControllerProviding, animated: Bool)
}

class Coordinator: Coordinating {

    var rootNavController: UINavigationController?

    required init(_ window: UIWindow?) {
        self.rootNavController = UINavigationController()
        window?.rootViewController = rootNavController
    }

    func present(_ viewModel: ViewControllerProviding, animated: Bool) {
        self.rootNavController?.pushViewController(viewModel.controller, animated: animated)
    }
}

protocol ViewControllerProviding {
    var controller:UIViewController { get }
}

protocol Routing {
    func presentDetailView(for item: Item)
}

class Router: Routing {
    private var coordinator: Coordinating
    
    init(coordinator: Coordinating) {
        self.coordinator = coordinator
    }
    
    func presentDetailView(for item: Item) {
        let factory = DetailViewViewModelFactory()
        self.coordinator.present(factory.build(item: item), animated: true)
    }
}
