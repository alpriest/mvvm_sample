//
//  DetailViewController.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 06/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit
import AppCenterCrashes

class DetailViewController: UIViewController {
    @IBOutlet weak var text: UILabel!
    @IBOutlet weak var image: UIImageView!
    public var viewModel:DetailViewViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let viewModel = self.viewModel {
            self.text.text = viewModel.title
            self.image.loadAsync(fromUrl: viewModel.imageUrl)
            
            let tapper = UITapGestureRecognizer()
            tapper.numberOfTapsRequired = 1
            tapper.addTarget(self, action: #selector(DetailViewController.crash))
            self.view.addGestureRecognizer(tapper)
        }
    }
    
    @objc
    func crash(sender: NSObject) {
        MSCrashes.generateTestCrash()
    }
}

class DetailViewViewModel : ViewControllerProviding {
    private var model: Item
    var controller: UIViewController
    
    init(controller: UIViewController, model: Item) {
        self.controller = controller
        self.model = model
    }
    
    var title: String {
        get {
            return self.model.title
        }
    }
    
    var imageUrl: URL {
        get {
            return URL(string: self.model.imageUrl)!
        }
    }
}

class DetailViewViewModelFactory {
    func build(item: Item) -> DetailViewViewModel {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.loadDetailViewController()
        let viewModel = DetailViewViewModel(controller: controller, model: item)
        controller.viewModel = viewModel
        return viewModel
    }
}

