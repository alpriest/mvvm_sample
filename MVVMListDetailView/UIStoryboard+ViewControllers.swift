//
//  UIStoryboard+ViewControllers.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 07/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func loadListViewController() -> ListViewController {
        return self.instantiateViewController(withIdentifier: "ListView") as! ListViewController
    }
    
    func loadDetailViewController() -> DetailViewController {
        return self.instantiateViewController(withIdentifier: "DeteilView") as! DetailViewController
    }
}

