//
//  Item.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 07/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit


struct Item {
    var title:String
    var imageUrl:String
    var showImage:Bool
}

struct ItemViewModel {
    var title:String
    var imageUrl:URL
    var showImage:Bool
}
