//
//  AppDelegate.swift
//  MVVMListDetailView
//
//  Created by Alistair Priest on 06/02/2018.
//  Copyright © 2018 Alistair Priest. All rights reserved.
//

import UIKit
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        self.initAppCenter()

        let coordinator = Coordinator(window)
        let router = Router(coordinator: coordinator)

        let factory = ListViewViewModelFactory(router: router)
        coordinator.present(factory.build(items: ItemRepository().getItems()), animated: false)
        
        self.window?.makeKeyAndVisible()

        return true
    }
    
    func initAppCenter() {
        MSAppCenter.start("29666501-eb64-4cec-bcfa-6914b7df8f7e", withServices:[
            MSAnalytics.self,
            MSCrashes.self
            ])
    }

}


